# Phillips - iOS Application

## Contact Information

[Connect With Me On LinkedIn](https://www.linkedin.com/in/brianweissberg/)

[View My Resume](https://drive.google.com/file/d/1O-jICEyKEEPOYnY7UMpFaHyCZCGrhqjd/view)

[Personal Website](https://brianweissberg.wixsite.com/brianweissberg)

## Video Walkthrough

[![Phillips Demo App](http://img.youtube.com/vi/Bc7TW4hbd0Y/0.jpg)](https://www.youtube.com/watch?v=Bc7TW4hbd0Y "Phillips Demo App")

# PhillipsDemoProject Overview

The PhillipsDemoProject is an iOS application designed to display a list of auctions. It utilizes SwiftUI for its user interface, creating an app that allows users to browse through auctions, search for specific items, and filter their search results according to various criteria.

## AuctionsListView

<img src="./MarkdownResources/image1.PNG" alt="AuctionsListView" width="200"/>

<img src="./MarkdownResources/image2.PNG" alt="AuctionsListView" width="200"/>

The `AuctionsListView` is a SwiftUI view that acts as the main interface for users to interact with auction listings.

### Key Features
- **ViewModel Integration**: It leverages an `@StateObject` of `AuctionsViewModel` to manage its data.
- **Dynamic Listing**: Auctions are listed using a `ScrollView` combined with a `LazyVStack`.
- **Search Functionality**: Users can search for auctions from the navigation bar.
- **Filter Menu**: Includes a filter menu toggled by a button in the navigation bar.
- **Navigation to Details**: Each item links to `AuctionDetails` via a `NavigationLink`.
- **Custom Auction Item Views**: Auction items are displayed using `AuctionListItemView`.

### UI Components
- **Toolbar with Custom Buttons**: Features custom buttons and an invisibly placed button for layout balance.
- **Sheet Presentation**: The filter menu is displayed as a sheet overlay.

### Overall Functionality
The `AuctionsListView` serves as the central hub for auction interaction within the app.

## PhillipsDemoProject: AuctionDetails Overview

<img src="./MarkdownResources/image3.PNG" alt="AuctionsListView" width="200"/>

<img src="./MarkdownResources/image4.PNG" alt="AuctionsListView" width="200"/>

<img src="./MarkdownResources/image8.PNG" alt="AuctionsListView" width="200"/>

<img src="./MarkdownResources/image7.PNG" alt="AuctionsListView" width="200"/>

The `AuctionDetails` view provides comprehensive details about auction items.

### Key Features and Components
- **AuctionDetailsViewModel**: Supplies data needed for the view.
- **UI Design**: 
  - Main Image: The main image of the item is prominently displayed.
  - Description and Details: Detailed and visually appealing presentation of the item's information.
  - Live Auction Details: Additional details for live auctions.
  - Registration Button: Styled call-to-action button.
  - Auction Item News: `AuctionItemNewsView` provides related news.
  - YouTube Video: Embedded video related to the item.
  - Other Auction Items Grid: Additional items are displayed in a grid.

### Navigation and Layout
- **Navigation Bar Customization**: Features the Phillips logo.
- **Content Layout**: Organized content in a `ScrollView` and nested `VStacks`.

### Functionality
The `AuctionDetails` view acts as an immersive portal into the specifics of an auction item.

## PhillipsDemoProject: BidsList Overview

<img src="./MarkdownResources/image5.PNG" alt="AuctionsListView" width="200"/>

The `BidsList` view is for users interested in the bidding aspect of auctions.

### Key Features and Components
- **User Interaction**: A registration call-to-action is prominently styled at the top.
- **Navigation and Information**: Divided sections for "My Bids" and "Past Bids".
- **Educational Content**: The `HowToBuyView` provides bidding information.

### Design and Layout
- **List-Based Layout**: Content is organized in a list for easy navigation.
- **Customization and Styling**: The registration button is customized with the Phillips brand colors.

### Functionality
The `BidsList` view is designed to engage users throughout their auction journey.

## PhillipsDemoProject: LoginView Overview

<img src="./MarkdownResources/image6.PNG" alt="AuctionsListView" width="200"/>

The `LoginView` facilitates user authentication with an intuitive interface.

### Key Features and Components
- **User Input Handling**: Users input their email and password.
- **Password Visibility Toggle**: Feature to toggle password visibility.
- **UI Design Elements**: 
  - Branding with Logo: Phillips logo is prominently displayed.
  - Styling and Layout: Input fields are styled for clarity and accessibility.
- **Action Buttons**: Includes a "Forgot your password?" and a "Sign In" button.
- **Accessibility and User Experience**: Responsive design and clear feedback points.

### Functionality
The `LoginView` acts as the gateway to the app, balancing security with user-friendly design.








