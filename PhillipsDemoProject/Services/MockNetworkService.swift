//
//  MockNetworkService.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/20/24.
//

import Foundation

// Simulates network service for fetching mock data from local JSON files.
class MockNetworkService {
    // Loads JSON data from a file in the app bundle.
    static func loadJsonData(for fileName: MockJson) throws -> Data {
        // Attempt to find the JSON file in the app bundle.
        guard let url = Bundle.main.url(forResource: fileName.rawValue, withExtension: "json") else {
            // If the file cannot be found, throw an error.
            throw MockNetworkServiceError.invalidJsonFile
        }
        
        // Attempt to load the file's data.
        let data = try Data(contentsOf: url)
        return data
    }
}

// Defines the possible JSON files that can be loaded.
enum MockJson: String {
    case auctions
    case auctionItems
    case newsItem
    case biddingInstructions
}

// Custom error type for errors related to the mock network service.
enum MockNetworkServiceError: Error {
    case invalidJsonFile // Indicates the specified JSON file does not exist.
}
