//
//  PhillipsDemoProjectApp.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/19/24.
//

import SwiftUI

@main
struct PhillipsDemoProjectApp: App {
    var body: some Scene {
        WindowGroup {
            MainTabView()
        }
    }
}
