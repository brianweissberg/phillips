//
//  MainTabView.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/19/24.
//

import SwiftUI

// Struct to hold string values used in MainTabView
private struct TabStrings {
    static let auctions = "Auctions"
    static let bids = "Bids"
    static let account = "Account"
    static let favorites = "Favorites"
    static let more = "More"
    
    // Image system names
    struct ImageNames {
        static let auctions = "photo.artframe.circle.fill"
        static let bids = "dollarsign.circle.fill"
        static let account = "person.crop.circle.fill"
        static let favorites = "heart.circle.fill"
        static let more = "ellipsis.circle"
    }
}

// Main container view for the app's tab-based navigation.
struct MainTabView: View {
    var body: some View {
        TabView {
            AuctionsListView()
                .tabItem {
                    Image(systemName: TabStrings.ImageNames.auctions)
                    Text(TabStrings.auctions)
                }
            
            BidsList()
                .tabItem {
                    Image(systemName: TabStrings.ImageNames.bids)
                    Text(TabStrings.bids)
                }
            
            LoginView()
                .tabItem {
                    Image(systemName: TabStrings.ImageNames.account)
                    Text(TabStrings.account)
                }
            
            FavoritesList()
                .tabItem {
                    Image(systemName: TabStrings.ImageNames.favorites)
                    Text(TabStrings.favorites)
                }
            
            MoreList()
                .tabItem {
                    Image(systemName: TabStrings.ImageNames.more)
                    Text(TabStrings.more)
                }
        }
        // Customize the toolbar color scheme for the tab bar.
        .toolbarColorScheme(.light, for: .tabBar)
    }
}
