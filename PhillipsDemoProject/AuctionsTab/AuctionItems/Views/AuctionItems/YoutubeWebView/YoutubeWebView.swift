//
//  YoutubeWebView.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/22/24.
//

import SwiftUI
import WebKit

private struct Constants {
    static let autoplay = "autoplay"
    static let mute = "mute"
    static let trueValue = "1"
}

// SwiftUI wrapper for WKWebView to display YouTube videos.
struct YoutubeWebView: UIViewRepresentable {
    let urlString: String // Base URL string of the YouTube video.

    // Creates WKWebView configured for autoplaying and muting the video.
    func makeUIView(context: Context) -> WKWebView {
        var config = URLComponents(string: urlString)
        let autoplayQueryItem = URLQueryItem(name: Constants.autoplay, value: Constants.trueValue)
        let muteQueryItem = URLQueryItem(name: Constants.mute, value: Constants.trueValue)
        config?.queryItems = [autoplayQueryItem, muteQueryItem]
        
        guard let url = config?.url else {
            return WKWebView() // Fallback to an empty WKWebView if URL is invalid.
        }

        let configuration = WKWebViewConfiguration()
        configuration.allowsInlineMediaPlayback = true // Allow inline video playback.
        
        let webView = WKWebView(frame: .zero, configuration: configuration)
        webView.load(URLRequest(url: url))
        return webView
    }

    // Updates the WKWebView with new state or data.
    func updateUIView(_ uiView: WKWebView, context: Context) {}
}
