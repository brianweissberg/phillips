//
//  AuctionItemsGridView.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/21/24.
//

import SwiftUI

// Grid view displaying auction items in two columns.
struct AuctionItemsGridView: View {
    @ObservedObject var viewModel: AuctionItemsViewModel
    
    var body: some View {
        ScrollView {
            HStack(alignment: .top) {
                
                // Left column for auction items
                VStack(alignment: .leading, spacing: 10) {
                    ForEach(viewModel.leftColumnItems) { item in
                        itemContentView(item: item)
                    }
                }
                
                // Right column for auction items
                VStack(alignment: .leading, spacing: 10) {
                    ForEach(viewModel.rightColumnItems) { item in
                        itemContentView(item: item)
                    }
                }
            }
            .padding()
        }
        // Disables scrolling to keep the grid fixed.
        .disabled(true)
    }
    
    // Helper view to create the content for each item.
    @ViewBuilder
    private func itemContentView(item: AuctionItemViewModel) -> some View {
        VStack(alignment: .leading, spacing: 2) {
            ZStack(alignment: .topTrailing) {
                Image(item.imageName)
                    .resizable()
                    .scaledToFit()
                
                // Overlay with item number.
                Circle()
                    .fill(Color.gray)
                    .frame(width: 25, height: 25)
                    .overlay(Text("\(item.itemNumber)")
                        .foregroundColor(.black)
                        .font(.system(size: 10, weight: .bold))
                    )
                    .opacity(0.5)
                    .offset(x: -10, y: 10) 
            }
            
            // Description and estimate for the auction item.
            VStack(alignment: .leading, spacing: 5) {
                Text(item.shortDescription)
                    .font(.system(size: 16))
                    .multilineTextAlignment(.leading)
                
                Text(item.longDescription)
                    .font(.system(size: 10))
                    .multilineTextAlignment(.leading)
                    .lineLimit(Int.random(in: 3...5))
                
                Text(item.usdEstimate)
                    .font(.system(size: 14))
                    .foregroundStyle(.secondary)
            }
            .padding()
            
        }
        // Styling for card-like appearance.
        .cornerRadius(10)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color(red: 0.8, green: 0.8, blue: 0.8), lineWidth: 1)
        )
        .clipShape(RoundedRectangle(cornerRadius: 10))
    }
}
