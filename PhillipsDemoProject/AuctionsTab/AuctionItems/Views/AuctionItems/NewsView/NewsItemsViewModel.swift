//
//  NewsItemsViewModel.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/22/24.
//

import Foundation

// Modify AuctionItemsViewModel
class NewsItemsViewModel: ObservableObject {
    @Published private(set) var newsItem: NewsItemModel?
    
    // Initializes the ViewModel and fetches auction items.
    init() {
        try? fetchNewsItems()
    }
    
    // Fetches auction items from a JSON file and decodes them into view models.
    private func fetchNewsItems() throws {
        let data = try MockNetworkService.loadJsonData(for: .newsItem)
        
        do {
            newsItem = try JSONDecoder().decode(NewsItemModel.self, from: data)
        } catch let error {
            print(error.localizedDescription)
        }
    }
}
