//
//  AuctionItemNewsView.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/22/24.
//

import SwiftUI

// A view that displays a featured news item related to an auction.
struct AuctionItemNewsView: View {
    @ObservedObject var viewModel: NewsItemsViewModel // Inject ViewModel
    
    var body: some View {
        if let newsItem = viewModel.newsItem {
            VStack {
                // The news item image
                Image(newsItem.image)
                    .resizable()
                    .scaledToFit()
                    .clipped()
                
                // The text content of the news item
                VStack(alignment: .leading, spacing: 8) {
                    // Section title
                    Text(newsItem.section)
                        .font(.system(size: 20, weight: .light))
                        .foregroundColor(.gray)
                    
                    // News item headline
                    Text(newsItem.headline)
                        .font(.headline)
                        .fontWeight(.semibold)
                    
                    // Description of the news item
                    Text(newsItem.description)
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                    
                    // Call-to-action button to read the full story
                    Button(action: {} ) {
                        Text(newsItem.readButtonText)
                            .font(.subheadline)
                            .foregroundColor(.blue)
                    }
                }
                .padding()
            }
            // Styling for the card-like appearance
            .cornerRadius(10)
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color(red: 0.8, green: 0.8, blue: 0.8), lineWidth: 1) // Light gray border
            )
            .clipShape(RoundedRectangle(cornerRadius: 10))
        }
    }
}
