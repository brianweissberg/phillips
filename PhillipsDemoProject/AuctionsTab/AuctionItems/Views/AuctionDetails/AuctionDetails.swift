//
//  AuctionDetails.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/21/24.
//

import SwiftUI
import WebKit

// Displays the detailed view of an auction item.
struct AuctionDetails: View {
    var viewModel: AuctionDetailsViewModel // ViewModel providing the details for the auction.
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                
                // Display the main image of the auction item.
                Image(viewModel.imageName)
                    .resizable()
                    .scaledToFit()
                
                // Display the description and auction details.
                VStack(alignment: .leading, spacing: 5) {
                    Text(viewModel.description)
                        .font(.system(size: 20, weight: .semibold))
                        .multilineTextAlignment(.leading)
                    
                    // Conditionally display live auction details if applicable.
                    if viewModel.auctionType == .liveAuction {
                        LiveAuctionDetailsView(viewModel: LiveAuctionDetailsViewModel(location: viewModel.location, startDate: viewModel.startDate, startTime: viewModel.startTime))
                    }
                }
                .padding(.horizontal)
                .padding(.top, 10)
                
                // Button for user action to register for the auction.
                Button(action: {}) {
                    Text(viewModel.registerButtonTitle)
                        .foregroundColor(.white)
                        .frame(maxWidth: .infinity)
                        .padding()
                        .background(.black)
                        .cornerRadius(10)
                        .padding()
                }
                
                // Include a news view related to the auction item.
                AuctionItemNewsView(viewModel: NewsItemsViewModel())
                    .padding([.horizontal, .top])
                
                // Displays a YouTube video in a WebView with dynamic sizing and rounded corners.
                VStack(alignment: .center) {
                    YoutubeWebView(urlString: viewModel.youtubeVideoUrl)
                        .frame(width: UIScreen.main.bounds.width - 40, // Adjusts the width to the screen width with some padding
                               height: (UIScreen.main.bounds.width - 40) * 9 / 16) // Calculates the height based on a 16:9 aspect ratio
                        .cornerRadius(10)
                }
                .padding([.top, .horizontal])
                
                // Grid view of other auction items.
                AuctionItemsGridView(viewModel: AuctionItemsViewModel())
            }
            .frame(maxWidth: .infinity, alignment: .leading) // Ensure content fills the available width.
            .navigationBarTitleDisplayMode(.inline) // Display the title inline with the navigation bar.
            .toolbar {
                ToolbarItem(placement: .principal) {
                    // Displays the Phillips logo in the navigation bar.
                    Image(viewModel.phillipsLogoImageName)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(maxWidth: UIScreen.main.bounds.width / 3)
                }
            }
        }
    }
}
