//
//  AuctionDetailsViewModel.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/21/24.
//

import Foundation

class AuctionDetailsViewModel {
    let auctionType: AuctionType
    let imageName: String
    let description: String
    let location: String
    let startDate: String
    let startTime: String
    let registerButtonTitle: String
    let phillipsLogoImageName: String
    let youtubeVideoUrl: String
    
    init(auction: AuctionViewModel) {
        self.auctionType = auction.auctionType
        self.imageName = auction.imageName
        self.description = auction.description
        self.location = auction.location
        self.startDate = auction.startDate
        self.startTime = auction.startTime
        self.registerButtonTitle = auction.registerButtonTitle
        self.phillipsLogoImageName = auction.phillipsLogoImageName
        self.youtubeVideoUrl = auction.youtubeVideoUrl
    }
}
