//
//  AuctionItemViewModel.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/21/24.
//

import Foundation

class AuctionItemViewModel: Identifiable {
    var id = UUID()
    let itemNumber: Int
    let imageName: String
    let shortDescription: String
    let longDescription: String
    let hkEstimate: String
    let usdEstimate: String
    
    init(item auctionItem: AuctionItem) {
        self.itemNumber = auctionItem.itemNumber
        self.imageName = auctionItem.imageName
        self.shortDescription = auctionItem.shortDescription
        self.longDescription = auctionItem.longDescription
        self.hkEstimate = auctionItem.hkEstimate
        self.usdEstimate = auctionItem.usdEstimate
    }
}
