//
//  AuctionItemsViewModel.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/21/24.
//

import Foundation

// ViewModel for managing auction items and splitting them into two columns for display.
class AuctionItemsViewModel: ObservableObject {
    @Published var auctionItems: [AuctionItemViewModel] = []
    
    // Get items for the left column based on even indexes.
    var leftColumnItems: [AuctionItemViewModel] {
        auctionItems.enumerated().filter { $0.offset % 2 == 0 }.map { $0.element }
    }
    
    // Get items for the right column based on odd indexes.
    var rightColumnItems: [AuctionItemViewModel] {
        auctionItems.enumerated().filter { $0.offset % 2 != 0 }.map { $0.element }
    }
    
    // Initializes the ViewModel and fetches auction items.
    init() {
        try? fetchAuctionItems()
    }
    
    // Fetches auction items from a JSON file and decodes them into view models.
    private func fetchAuctionItems() throws {
        let data = try MockNetworkService.loadJsonData(for: .auctionItems)
        
        do {
            let response = try JSONDecoder().decode([AuctionItem].self, from: data)
            auctionItems = response.compactMap { AuctionItemViewModel(item: $0) }
        } catch let error {
            print(error.localizedDescription)
        }
    }
}
