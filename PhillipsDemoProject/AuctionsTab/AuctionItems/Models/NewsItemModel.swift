//
//  NewsItemModel.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/22/24.
//

import Foundation

struct NewsItemModel: Decodable, Identifiable {
    var id = UUID()
    let image: String
    let section: String
    let headline: String
    let description: String
    let readButtonText: String
    
    enum CodingKeys: String, CodingKey {
        case image, section, headline, description, readButtonText
    }
}
