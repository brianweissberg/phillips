//
//  AuctionItem.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/21/24.
//

import Foundation

struct AuctionItem: Decodable, Identifiable {
    var id = UUID()
    let itemNumber: Int
    let imageName: String
    let brand: String?
    let shortDescription: String
    let longDescription: String
    let hkEstimate: String
    let usdEstimate: String
    
    enum CodingKeys: String, CodingKey {
        case itemNumber, imageName, brand, shortDescription, longDescription, hkEstimate, usdEstimate
    }
}
