//
//  LiveAuctionDetailsView.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/19/24.
//

import SwiftUI

// Defines constants for image names used in the view.
private struct Constants {
    static let mapImage = "mappin.circle"
    static let calendarImage = "calendar.circle"
    static let clockImage = "clock"
}

// ViewModel for live auction details including location, start date, and start time.
class LiveAuctionDetailsViewModel {
    let location: String
    let startDate: String
    let startTime: String
    
    init(location: String, startDate: String, startTime: String) {
        self.location = location
        self.startDate = startDate
        self.startTime = startTime
    }
}

// View that displays details for a live auction, including location, date, and time.
struct LiveAuctionDetailsView: View {
    let viewModel: LiveAuctionDetailsViewModel
    
    var body: some View {
        HStack {
            
            // Location information with icon.
            HStack {
                Image(systemName: Constants.mapImage)
                Text(viewModel.location)
                    .font(.system(size: 14))
            }
            
            // Start date information with icon.
            HStack {
                Image(systemName: Constants.calendarImage)
                Text(viewModel.startDate)
                    .font(.system(size: 14))
            }
            
            // Start time information with icon.
            HStack {
                Image(systemName: Constants.clockImage)
                Text(viewModel.startTime)
                    .font(.system(size: 14))
            }
            
            Spacer() // Ensures content is aligned to the left.
        }
    }
}
