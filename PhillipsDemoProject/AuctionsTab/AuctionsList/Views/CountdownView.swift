//
//  CountdownView.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/20/24.
//

import SwiftUI

private struct Constants {
    static let timeFormat = "%02d"
    static let timeRemainaing = "0d 00h 00m 00s"
}

// Displays a countdown to a future date.
struct CountdownView: View {
    let futureDate: Date // The future date to count down to.
    @State private var timeRemaining = "" // String to display the time remaining.
    
    // Timer that updates every second.
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    var body: some View {
        Text(timeRemaining)
            .onAppear(perform: updateRemainingTime) // Initializes the countdown.
            .onReceive(timer) { _ in
                updateRemainingTime() // Updates the countdown every second.
            }
    }
    
    // Calculates the time remaining until the future date and updates the display.
    private func updateRemainingTime() {
        let now = Date()
        let remaining = futureDate.timeIntervalSince(now)
        
        if remaining > 0 {
            // Break down the total seconds into days, hours, minutes, and seconds.
            let days = Int(remaining) / 86400
            let hours = Int(remaining) / 3600 % 24
            let minutes = Int(remaining) / 60 % 60
            let seconds = Int(remaining) % 60
            // Update the display format.
            timeRemaining = "\(days)d \(String(format: Constants.timeFormat, hours))h \(String(format: Constants.timeFormat, minutes))m \(String(format: Constants.timeFormat, seconds))s"
        } else {
            timeRemaining = Constants.timeRemainaing
            timer.upstream.connect().cancel() // Stop the timer if the date has passed.
        }
    }
}
