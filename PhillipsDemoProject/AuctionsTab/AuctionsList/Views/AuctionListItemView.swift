//
//  AuctionListItemView.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/22/24.
//

import SwiftUI

// Represents a single item in the auction list, displaying key details.
struct AuctionListItemView: View {
    let viewModel: AuctionViewModel
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                
                // Display auction item image.
                Image(viewModel.imageName)
                    .resizable()
                    .scaledToFit()
                
                // Auction details and description.
                VStack(alignment: .leading, spacing: 5) {
                    
                    // Reusable view for displaying auction description.
                    AuctionDescriptionView(auction: viewModel)
                    
                    // Display additional details based on auction type.
                    switch viewModel.auctionType {
                    case .liveAuction:
                        
                        // ViewModel for live auction details.
                        let detailsViewModel = LiveAuctionDetailsViewModel(location: viewModel.location, startDate: viewModel.startDate, startTime: viewModel.startTime)
                        
                        // View displaying live auction details.
                        LiveAuctionDetailsView(viewModel: detailsViewModel)
                    case .timedAuction:
                        
                        // View displaying countdown for timed auctions.
                        CountdownView(futureDate: viewModel.generateRandomFutureDate())
                    }
                }
                .padding(.horizontal)
            }
        }
        .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading) // Ensures the view stretches across the available width.
    }
}
