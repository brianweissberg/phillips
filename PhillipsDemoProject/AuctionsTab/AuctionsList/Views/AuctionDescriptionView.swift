//
//  AuctionDescriptionView.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/20/24.
//

import SwiftUI
import Foundation

// Displays the description and type of an auction.
struct AuctionDescriptionView: View {
    @State var auction: AuctionViewModel
    
    var body: some View {
        VStack(alignment: .leading, spacing: 5) {
            
            // Displays the auction's description.
            Text(auction.description)
                .font(.system(size: 20, weight: .light))
                .multilineTextAlignment(.leading)
            
            // Displays the auction's type (e.g., Live Auction, Timed Auction).
            Text(auction.auctionType.rawValue)
                .font(.system(size: 16))
                .foregroundStyle(.gray)
        }
    }
}

