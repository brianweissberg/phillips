//
//  AuctionsListView.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/19/24.
//

import SwiftUI

// Defines constants for image names used in the view.
private struct Constants {
    static let defaultSelectedFilter = "Upcoming"
    static let searchAuctions = "Search Auctions..."
    static let filterSystemImage = "line.horizontal.3.decrease.circle"
    static let phillipsLogo = "phillipsLogo"
}

// Displays a list of auctions using a view model.
struct AuctionsListView: View {
    @StateObject var viewModel = AuctionsViewModel() // ViewModel for auction data.
    @State private var showingFilterMenu = false // Controls the display of the filter menu.
    @State private var selectedFilter: String = Constants.defaultSelectedFilter // Selected filter option.
    
    var body: some View {
        NavigationStack {
            ScrollView {
                
                // Stacks auction items vertically with spacing.
                LazyVStack(spacing: 20) {
                    ForEach(viewModel.filteredAuctions) { auction in
                        
                        // Creates a navigation link for each auction item.
                        NavigationLink(destination: AuctionDetails(viewModel: AuctionDetailsViewModel(auction: auction))) {
                            
                            // Custom view for each auction item.
                            AuctionListItemView(viewModel: auction)
                        }
                        .buttonStyle(PlainButtonStyle()) // Removes the default button style.
                    }
                }
            }
            .searchable(text: $viewModel.searchText, prompt: Constants.searchAuctions) // Adds a search bar.
            .navigationBarTitleDisplayMode(.inline) // Sets the navigation bar title display mode.
            .edgesIgnoringSafeArea(.horizontal) // Allows content to extend into the horizontal safe area.
            .animation(.spring(duration: 0.3), value: viewModel.filteredAuctions) // Adds animation when auctions list updates.
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    
                    // Button to toggle the filter menu.
                    Button(action: {
                        showingFilterMenu.toggle()
                    }) {
                        Image(systemName: Constants.filterSystemImage)
                            .resizable()
                            .scaledToFit()
                    }
                }
                
                ToolbarItem(placement: .principal) {
                    // Displays the Phillips logo in the navigation bar.
                    Image(Constants.phillipsLogo)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(maxWidth: UIScreen.main.bounds.width / 3)
                }
                
                // An invisible item to ensure the logo remains centered.
                ToolbarItem(placement: .navigationBarTrailing) {
                    Image(systemName: Constants.filterSystemImage)
                        .resizable()
                        .scaledToFit()
                        .opacity(0) // Invisible but takes up space.
                        .disabled(true)
                }
            }
            .sheet(isPresented: $showingFilterMenu) {
                FilterMenuView(selectedFilter: $selectedFilter) // Displays the filter menu.
            }
        }
    }
}
