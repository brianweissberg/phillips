//
//  FilterMenuView.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/22/24.
//

import SwiftUI

private struct Constants {
    static let auctions = "Auctions"
    static let done = "Done"
    static let upcoming = "Upcoming"
    static let past = "Past"
}

// A view for selecting an auction filter.
struct FilterMenuView: View {
    @Environment(\.presentationMode) var presentationMode // Allows dismissal of the view.
    @Binding var selectedFilter: String // Binding to the selected filter option.
    let options = [Constants.upcoming, Constants.past] // Filter options.
    
    var body: some View {
        NavigationView {
            Form {
                
                // Picker for selecting a filter option.
                Picker(Constants.auctions, selection: $selectedFilter) {
                    ForEach(options, id: \.self) {
                        Text($0)
                    }
                }
                .pickerStyle(.inline)
            }
            .toolbar {
                
                // Toolbar item for a 'Done' button to dismiss the filter menu.
                ToolbarItem(placement: .confirmationAction) {
                    Button(Constants.done) {
                        presentationMode.wrappedValue.dismiss()
                    }
                }
            }
        }
    }
}
