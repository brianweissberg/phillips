//
//  AuctionsViewModel.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/20/24.
//

import Foundation
import Combine

// ViewModel to manage auction data and perform search filtering.
class AuctionsViewModel: ObservableObject {
    @Published var auctions: [AuctionViewModel] = [] // All auctions.
    @Published var searchText = "" // The current search text.
    @Published var filteredAuctions: [AuctionViewModel] = [] // Auctions filtered by search.
    private var cancellables = Set<AnyCancellable>() // Holds Combine cancellables.
    
    // Fetches auctions from a data source and updates published properties.
    func fetchAuctions() throws {
        let data = try MockNetworkService.loadJsonData(for: .auctions)
        let response = try JSONDecoder().decode([Auction].self, from: data)
        let mapped = response.compactMap { AuctionViewModel(auction: $0) }
        DispatchQueue.main.async {
            self.auctions = mapped
            self.filteredAuctions = mapped
        }
    }
    
    // Filters the auctions based on the search text.
    func search() {
        if searchText.isEmpty {
            filteredAuctions = auctions
        } else {
            var result: [AuctionViewModel] = []
            for auction in auctions {
                if auction.description.localizedCaseInsensitiveContains(searchText) ||
                    auction.imageName.localizedCaseInsensitiveContains(searchText) {
                    result.append(auction)
                }
            }
            filteredAuctions = result
        }
    }
    
    // Initializes the ViewModel, fetches data, and sets up search text observation.
    init() {
        try? fetchAuctions()
        $searchText
            .debounce(for: .milliseconds(300), scheduler: RunLoop.main) // Delays processing to avoid rapid updates.
            .removeDuplicates() // Prevents searching for the same text multiple times.
            .sink(receiveValue: { _ in
                self.search()
            })
            .store(in: &cancellables) // Stores the cancellable to manage the subscription's lifecycle.
    }
}
