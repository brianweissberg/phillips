//
//  AuctionViewModel.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/20/24.
//

import Foundation
import UIKit

// Defines the types of auctions.
enum AuctionType: String {
    case liveAuction = "Live Auction"
    case timedAuction = "Timed Auction"
}

// ViewModel for an individual auction.
class AuctionViewModel: Identifiable, Equatable {
    var id = UUID()
    let auctionType: AuctionType
    let description: String
    let imageName: String
    let startDate: String
    let startTime: String
    let registerButtonTitle: String
    let phillipsLogoImageName: String
    let youtubeVideoUrl: String
    private(set) var location: String // Location can be set only within this class.
    
    // Initializes the ViewModel with an Auction model.
    init(auction: Auction) {
        self.description = auction.description
        self.imageName = auction.imageName
        self.location = auction.location
        self.startDate = auction.startDate
        self.startTime = auction.startTime
        self.registerButtonTitle = auction.registerButtonTitle
        self.phillipsLogoImageName = auction.phillipsLogo
        self.youtubeVideoUrl = auction.youtubeVideoUrl
        
        // Determines the auction type based on the model's value.
        switch auction.auctionType {
        case AuctionType.liveAuction.rawValue:
            self.auctionType = .liveAuction
        case AuctionType.timedAuction.rawValue:
            self.auctionType = .timedAuction
        default:
            self.auctionType = .liveAuction // Default to live auction.
        }
    }
    
    // Generates a random future date for demo purposes.
    func generateRandomFutureDate() -> Date {
        let today = Date()
        let randomDays = Int.random(in: 1...30)
        let randomHours = Int.random(in: 0...23)
        let randomMinutes = Int.random(in: 0...59)
        let randomSeconds = Int.random(in: 0...59)
        
        var components = DateComponents()
        components.day = randomDays
        components.hour = randomHours
        components.minute = randomMinutes
        components.second = randomSeconds
        
        return Calendar.current.date(byAdding: components, to: today) ?? today
    }
}

// Conformance to Equatable for comparison.
extension AuctionViewModel {
    static func == (lhs: AuctionViewModel, rhs: AuctionViewModel) -> Bool {
        // Compares all relevant properties for equality.
        return lhs.description == rhs.description &&
        lhs.imageName == rhs.imageName &&
        lhs.startDate == rhs.startDate &&
        lhs.startTime == rhs.startTime &&
        lhs.location == rhs.location &&
        lhs.auctionType == rhs.auctionType
    }
}
