//
//  Auction.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/20/24.
//

import Foundation

struct Auction: Decodable {
    let auctionType: String
    let description: String
    let imageName: String
    let location: String
    let startDate: String
    let startTime: String
    let registerButtonTitle: String
    let phillipsLogo: String
    let youtubeVideoUrl: String
}
