//
//  FavoritesList.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/21/24.
//

import SwiftUI

struct FavoritesList: View {
    var body: some View {
        List {
            Text("My Favorites")
            Text("My Artists")
        }
    }
}
