import SwiftUI

// LoginView presents a user interface for entering login credentials.
struct LoginView: View {
    // State variables to capture user input and toggle password visibility
    @State private var username: String = ""
    @State private var password: String = ""
    @State private var isPasswordVisible: Bool = false
    
    var body: some View {
        ZStack {
            Color(.systemBackground)
                .edgesIgnoringSafeArea(.all)
            
            VStack(spacing: 20) {
                // Logo at the top of the login view
                Image("phillipsLogo")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(maxWidth: UIScreen.main.bounds.width / 2)
                
                // Text field for the username input
                TextField("Email", text: $username)
                    .padding()
                    .background(Color(.systemGray6))
                    .cornerRadius(10)
                    .padding(.horizontal)
                
                // HStack allows for a password toggle button within the text field
                HStack {
                    if isPasswordVisible {
                        TextField("Password", text: $password)
                    } else {
                        SecureField("Password", text: $password)
                    }
                    // Toggle button to show/hide the password
                    Button(action: {
                        isPasswordVisible.toggle()
                    }) {
                        Image(systemName: isPasswordVisible ? "eye.slash.fill" : "eye.fill")
                            .foregroundColor(.gray)
                    }
                }
                .padding()
                .background(Color(.systemGray6))
                .cornerRadius(10)
                .padding(.horizontal)
                
                // Button for users who forgot their password
                Button("Forgot your password?") {}
                    .foregroundColor(.blue)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding([.leading, .trailing], 20)
                
                // Sign in button
                Button(action: {}) {
                    Text("Sign In")
                        .foregroundColor(.white)
                        .frame(maxWidth: .infinity)
                        .padding()
                        .background(.black)
                        .cornerRadius(10)
                        .padding(.horizontal)
                }
                
                // Option to sign up for a new account
                HStack(spacing: 5) {
                    Text("Don't have an account?")
                        .foregroundColor(.gray)
                    
                    Button("Sign up now") {}
                        .foregroundColor(.blue)
                    
                    Spacer()
                }
                .padding(.leading, 20)
                .padding(.top, 10)
                
                // Pushes all content to the top
                Spacer()
            }
            .padding(.top)
        }
    }
}
