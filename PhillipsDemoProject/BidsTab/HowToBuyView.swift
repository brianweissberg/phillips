//
//  HowToBuyView.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/22/24.
//

import SwiftUI

// Provides information on how to participate in auctions.
struct HowToBuyView: View {

    @ObservedObject var viewModel = BiddingInstructionsViewModel()
    
    var body: some View {
        ScrollView {
            // Content is aligned to the leading edge.
            VStack(alignment: .leading, spacing: 10) {
                if let biddingInstructions = viewModel.biddingInstructions {
                    // Title for the "How Bidding Works" section.
                    Text(viewModel.biddingInstructions?.title ?? "How Bidding Works")
                        .font(.title)
                        .fontWeight(.semibold)
                        .padding([.top, .bottom])
                        .foregroundColor(.black)
                    
                    
                    // Subheading for auction types and bidding.
                    Text(biddingInstructions.auctionTypesAndBidding)
                        .font(.title3)
                        .fontWeight(.semibold)
                        .padding(.bottom)
                    
                    // Description and instructions for different auction types.
                    Text(biddingInstructions.pleaseNote)
                    Text(biddingInstructions.liveAuctions)
                    Text(biddingInstructions.timedAuctions)
                    Text(biddingInstructions.absenteeBidding)
                    
                    // A tip for users to save time.
                    Text(biddingInstructions.tip)
                    
                    // Call-to-action button for users to learn more.
                    Button(action: {}) {
                        Text(biddingInstructions.learnMore)
                            .fontWeight(.semibold)
                            .foregroundColor(.blue)
                            .padding(.top)
                    }
                }
            }
            .padding() // Padding for the entire VStack content.
        }
        .navigationTitle("How to Buy") // Navigation bar title.
    }
}
