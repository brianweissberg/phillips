//
//  BiddingInstructionsViewModel.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/22/24.
//

import Foundation

class BiddingInstructionsViewModel: ObservableObject {
    @Published private(set) var biddingInstructions: BiddingInstructions?
    
    // Initializes the ViewModel and fetches bidding instructions.
    init() {
        try? fetchBiddingInstructions()
    }
    
    // Fetch bidding items from a JSON file and decodes into BiddingInstructions model
    private func fetchBiddingInstructions() throws {
        let data = try MockNetworkService.loadJsonData(for: .biddingInstructions)
        
        do {
            biddingInstructions = try JSONDecoder().decode(BiddingInstructions.self, from: data)
        } catch let error {
            print(error.localizedDescription)
        }
    }
}
