//
//  BidsList.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/21/24.
//

import SwiftUI

// Struct to hold string values used in BidsList
private struct BidsListStrings {
    static let registerToBid = "Register to Bid"
    static let bids = "Bids"
    static let myBids = "My Bids"
    static let pastBids = "Past Bids"
}

// Displays a list of bidding options and information on how to bid.
struct BidsList: View {
    var body: some View {
        List {
            // Registration call-to-action at the top of the list.
            Section {
                Button(action: {}) {
                    Text(BidsListStrings.registerToBid)
                        .foregroundColor(.white)
                        .frame(maxWidth: .infinity)
                        .padding()
                        .background(.black)
                        .cornerRadius(10)
                }
                .listRowBackground(Color.clear) // Makes the button's row background transparent.
            }
            
            // Section for navigating to bids the user has made or can view.
            Section(header: Text(BidsListStrings.bids)) {
                Text(BidsListStrings.myBids) // Navigates to the user's current bids.
                Text(BidsListStrings.pastBids) // Navigates to the user's past bids.
            }
            
            // Section that embeds the 'HowToBuyView' for additional information.
            Section {
                HowToBuyView() // Provides details on how to participate in auctions.
                    .frame(maxHeight: .infinity) // Allows the view to expand vertically as needed.
            }
        }
    }
}
