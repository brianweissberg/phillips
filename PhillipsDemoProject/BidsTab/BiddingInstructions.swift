//
//  BiddingInstructions.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/22/24.
//

import Foundation

struct BiddingInstructions: Decodable, Identifiable {
    var id = UUID()
    let title: String
    let auctionTypesAndBidding: String
    let pleaseNote: String
    let liveAuctions: String
    let timedAuctions: String
    let absenteeBidding: String
    let tip: String
    let learnMore: String
    
    enum CodingKeys: String, CodingKey {
        case title, auctionTypesAndBidding, pleaseNote, liveAuctions, timedAuctions, absenteeBidding, tip, learnMore
    }
}
