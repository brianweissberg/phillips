//
//  MoreList.swift
//  PhillipsDemoProject
//
//  Created by Brian Weissberg on 3/22/24.
//

import SwiftUI

struct MoreList: View {
    var body: some View {
        List {
            Text("Calendar")
            Text("Departments")
            Text("Private Sales")
            Text("Locations")
            Text("Consign")
        }
    }
}
